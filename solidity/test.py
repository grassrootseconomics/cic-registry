import os
import web3
import eth_tester
import json

raise NotImplementedError('test is broken. fix!')

eth_params = eth_tester.backends.pyevm.main.get_default_genesis_params({
    'gas_limit': 9000000,
    })
backend = eth_tester.PyEVMBackend(eth_params)
instance = eth_tester.EthereumTester(backend)
provider = web3.Web3.EthereumTesterProvider(instance)
w3 = web3.Web3(provider)


f = open('CICRegistry.bin', 'r')
bytecode = f.read()
f.close()

f = open('CICRegistry.json', 'r')
abi = json.load(f)
f.close()

c = w3.eth.contract(abi=abi, bytecode=bytecode)

identifiers = [
    web3.Web3.toHex(text='foo'),
    web3.Web3.toHex(text='bar'),
        ]
tx_hash = c.constructor(identifiers).transact({'from': w3.eth.accounts[0]})

r = w3.eth.getTransactionReceipt(tx_hash)


c = w3.eth.contract(abi=abi, address=r.contractAddress)

# check that identifiers were correctly registered after construction
identifiers = []
i = 0
while True:
    try:
        identifier = c.functions.identifiers(i).call()
        identifiers.append(identifier)
    except:
        break
    i += 1

assert identifiers[0][:3] == b'foo'
assert identifiers[1][:3] == b'bar'

# check that entries cannot be set by non-owner
addr_foo = os.urandom(20)
fail = False
try:
    c.functions.set(identifiers[0], addr_foo).transact({'from': w3.eth.accounts[1]})
except:
    fail = True
assert fail

# check that entries are correctly set
c.functions.set(identifiers[0], addr_foo).transact({'from': w3.eth.accounts[0]})
addr_foo_hex = web3.Web3.toChecksumAddress(web3.Web3.toHex(addr_foo))
assert addr_foo_hex == c.functions.entries(identifiers[0]).call()

addr_bar = os.urandom(20)
c.functions.set(identifiers[1], addr_bar).transact({'from': w3.eth.accounts[0]})
addr_bar_hex = web3.Web3.toChecksumAddress(web3.Web3.toHex(addr_bar))
assert addr_bar_hex == c.functions.entries(identifiers[1]).call()

# check that unknown identifiers cannot be added
addr_baz = os.urandom(20)
identifer_baz = b'baz'
fail = False
try:
    c.functions.set(identifier_baz, addr_baz).transact({'from': w3.eth.accounts[0]})
except:
    fail = True
assert fail

# check that entries cannot be updated after seal
addr_foo_ii = os.urandom(20)
c.functions.set(identifiers[0], addr_foo_ii).transact({'from': w3.eth.accounts[0]})
c.functions.seal().transact({'from': w3.eth.accounts[0]})
addr_foo_iii = os.urandom(20)
fail = False
try:
    c.functions.set(identifiers[0], addr_foo_iii).transact({'from': w3.eth.accounts[0]})
except:
    fail = True
assert fail

# check that seal set owner to zero-address
owner = c.functions.owner().call()
owner_bytes = bytes.fromhex(owner[2:])
assert int.from_bytes(owner_bytes, 'little') == 0
