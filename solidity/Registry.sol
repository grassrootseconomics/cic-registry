pragma solidity >0.6.11;

// Author:	Louis Holbrook <dev@holbrook.no> 0826EDA1702D1E87C6E2875121D2E7BB88C2A746
// SPDX-License-Identifier:	GPL-3.0-or-later
// File-version: 2
// Description: Top-level smart contract registry for the CIC network


contract CICRegistry {
	address public owner;

	bytes32[] public identifiers;
	mapping (bytes32 => address) entries;		// contractidentifier -> address
	mapping (bytes32 => bytes32) chainIdentifiers;	// contractidentifier -> chainidentifier
	mapping (bytes32 => bytes32) chainConfigs; 	// chainidentifier -> chainconfig

	constructor(bytes32[] memory _identifiers) public {
		owner = msg.sender;
		for (uint i = 0; i < _identifiers.length; i++) {
			identifiers.push(_identifiers[i]);
		}
	}

	function set(bytes32 _identifier, address _address, bytes32 _chainDescriptor, bytes32 _chainConfig) public returns (bool) {
		require(msg.sender == owner);
		bool found = false;
		for (uint i = 0; i < identifiers.length; i++) {
			if (identifiers[i] == _identifier) {
				found = true;
			}	
		}
		require(found);

		entries[_identifier] = _address;
		chainIdentifiers[_identifier] = _chainDescriptor;
		chainConfigs[_chainDescriptor] = _chainConfig;
		return true;
	}

	function seal() public returns (bool) {
		require(msg.sender == owner);
		owner = address(0);
		return true;
	}

	function addressOf(bytes32 _identifier) public view returns (address) {
		return entries[_identifier];
	}

	function chainOf(bytes32 _identifier) public view returns (bytes32) {
		return chainIdentifiers[_identifier];
	}

	function configSumOf(bytes32 _chain) public view returns (bytes32) {
		return chainConfigs[_chain];
	}
}
