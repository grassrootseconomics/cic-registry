FROM ethereum/solc:0.6.12

FROM python:3.8.6-alpine

COPY --from=0 /usr/bin/solc /usr/bin/solc

RUN apk update &&\
	apk add gcc bash musl-dev

WORKDIR /usr/src

# Try to keep everything above here re-usable!

COPY ./solidity/ /usr/src/cic_registry/solidity/
COPY ./python/ /usr/src/cic_registry/python/

RUN cd cic_registry/solidity && \
	solc Registry.sol --abi | awk 'NR>3' > Registry.abi.json

RUN cd cic_registry/solidity && \
	solc Registry.sol --bin | awk 'NR>3' > Registry.bin && \
	truncate -s "$((`stat -t -c "%s" Registry.bin`-1))" Registry.bin

RUN cd cic_registry/python && \
	pip install --extra-index-url https://pip.grassrootseconomics.net:8433 .

# To deploy:
# CIC_REGISTRY_ADDRESS=`cic-registry-deploy -i CICRegistry -i BancorRegistry -i AccountIndex`
# cic-registry-set -i CICRegistry -s Bloxberg:8995 -r $CIC_REGISTRY_ADDRESS $CIC_REGISTRY_ADDRESS
# cic-registry-set -i BancorRegistry -s Bloxberg:8995 -r $CIC_REGISTRY_ADDRESS $BANCOR_REGISTRY_ADDRESS
# cic-registry-set -i AccountRegistry -s Bloxberg:8995 -r $CIC_REGISTRY_ADDRESS $CIC_REGISTRY_ADDRESS
# cic-registry-seal -r $CIC_REGISTRY_ADDRESS
