# standard imports
import os

# third-party imports
import pytest

# local imports
from cic_registry import CICRegistry
from cic_registry.error import RoleExistsError


def test_roles(
    default_chain_spec,
    cic_registry,
        ):

    foo_account = '0x' + os.urandom(20).hex()

    # unknown contract identifier fails
    with pytest.raises(ValueError):
        CICRegistry.add_role(default_chain_spec, foo_account, 'Foo')

    # but if overridden it succeeds
    CICRegistry.add_role(default_chain_spec, foo_account, 'Foo', missing_contract_ok=True)

    # cannot register duplicate with same identifier
    with pytest.raises(RoleExistsError):
        CICRegistry.add_role(default_chain_spec, foo_account, 'Foo', missing_contract_ok=True)

    # cannot register duplicate with different identifier
    with pytest.raises(RoleExistsError):
        CICRegistry.add_role(default_chain_spec, foo_account, 'CICRegistry')

    # can register right away for known contract identifier
    registry_account = '0x' + os.urandom(20).hex()
    CICRegistry.add_role(default_chain_spec, registry_account, 'CICRegistry')


    # get them, too
    foo_role = CICRegistry.get_role(default_chain_spec, 'Foo')
    assert foo_role.address() == foo_account
    assert foo_role.name() == 'Foo'

    registry_role = CICRegistry.get_role(default_chain_spec, 'CICRegistry')
    assert registry_role.address() == registry_account
    assert registry_role.name() == 'CICRegistry'
