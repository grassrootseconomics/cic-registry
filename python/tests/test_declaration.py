# standard imports
import os

# third-party imports
import pytest

# local imports
from cic_registry.const import zero_content
from cic_registry.declaration import to_token_declaration
from cic_registry.error import UnknownDeclarationError


def test_declaration():

    declarator_address = '0x' + os.urandom(20).hex()
    target_address = '0x' + os.urandom(20).hex()
    declaration_items = [
            '0x' + os.urandom(32).hex(),
            '0x' + os.urandom(32).hex(),
            ]

    with pytest.raises(ValueError):
        to_token_declaration(declarator_address, target_address, [zero_content])


    with pytest.raises(UnknownDeclarationError):
        t = to_token_declaration(declarator_address, target_address, declaration_items[:1])
    
    with pytest.raises(UnknownDeclarationError):
        t = to_token_declaration(declarator_address, target_address, declaration_items)

    declaration_item_ok = '0x' + os.urandom(32).hex()
    def bogus_filter(src):
        if src != declaration_item_ok:
            raise UnknownDeclarationError
        return "foo"
  
    t = to_token_declaration(declarator_address, target_address, declaration_items + [declaration_item_ok], filters=[bogus_filter])
    assert t != None
    assert t.items[declarator_address][0] == "foo"

    t = to_token_declaration(declarator_address, target_address, [declaration_item_ok] + declaration_items, filters=[bogus_filter])
    assert t != None
    assert t.items[declarator_address][0] == "foo"

    def bogus_filter_two(src):
        if src not in declaration_items:
            raise UnknownDeclarationError
        return "bar" + src

    t = to_token_declaration(declarator_address, target_address, [declaration_item_ok] + declaration_items, filters=[bogus_filter_two])
    assert t != None
    assert len(t.items[declarator_address]) == 2
    assert t.items[declarator_address][0] == "bar" + declaration_items[0]
    assert t.items[declarator_address][1] == "bar" + declaration_items[1]

    t = to_token_declaration(declarator_address, target_address, [declaration_item_ok] + declaration_items, filters=[bogus_filter, bogus_filter_two])
    assert t != None
    assert len(t.items[declarator_address]) == 3
    assert t.items[declarator_address][0] == "foo"
    assert t.items[declarator_address][1] == "bar" + declaration_items[0]
    assert t.items[declarator_address][2] == "bar" + declaration_items[1]

