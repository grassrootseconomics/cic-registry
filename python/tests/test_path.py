# standard imports
import os
import tempfile

# third-party imports
from cic_registry import CICRegistry

script_dir = os.path.dirname(__file__)

def test_path():
    t = tempfile.mkstemp()
    b = os.path.basename(t[1])
    CICRegistry.add_path(os.path.dirname(t[1]))
    assert CICRegistry.search_path('ERC20.json') == None
    assert CICRegistry.search_path(b) == t[1]
    CICRegistry.add_path(os.path.join(script_dir, 'testdata', 'solidity'))
    assert CICRegistry.search_path('ERC20.json') != None
