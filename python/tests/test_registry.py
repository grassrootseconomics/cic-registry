# standard imports
import pytest
import os
import logging

# third-party imports
import pytest
import web3

# local imports
#from cic_registry import registry
from cic_registry import CICRegistry
from cic_registry.error import ContractExistsError
from cic_registry.error import UnknownContractError

logg = logging.getLogger()


def test_cic_registry_get_address(
        default_chain_registry,
        default_chain_spec,
        cic_registry,
        solidity_abis,
        w3,
        dummy_token,
        ):

    c = CICRegistry.get_address(default_chain_spec, cic_registry)
    assert c != None

    logg.debug('dummy {}'.format(dummy_token))
    with pytest.raises(UnknownContractError):
        c = CICRegistry.get_address(default_chain_spec, dummy_token)

    class Oracle:

        def open():
            pass

        def close():
            pass

        def get(address):
            abi = solidity_abis['ERC20']
            return abi

    default_chain_registry.add_oracle(Oracle, 'schmoracle')
    abi = default_chain_registry.search(dummy_token)
    assert abi == CICRegistry.abi('ERC20')

    c = CICRegistry.get_address(default_chain_spec, dummy_token)
    fn = c.function('symbol')
    fn().call() == 'DUM'


def test_cic_registry_get_contract(
        default_chain_spec,
        cic_registry,
        ):

    c = CICRegistry.get_contract(default_chain_spec, 'CICRegistry')
    assert c != None

    with pytest.raises(ValueError):
        CICRegistry.get_contract(default_chain_spec, 'Foo')


def test_cic_get_external_token(
        default_chain_spec,
        cic_registry,
        dummy_token,
        w3,
    ):

    try:
        c = CICRegistry.get_contract(default_chain_spec, 'TokenRegistry')
    except ValueError as e:
        pytest.skip('token registry contract not found, skipping test')
        

    c = w3.eth.contract(abi=CICRegistry.abi('ERC20'), address=dummy_token)
    symbol = c.functions.symbol().call()

    t = CICRegistry.get_token(default_chain_spec, symbol)
    assert t.symbol() == symbol
