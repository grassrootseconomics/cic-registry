# standard imports
import sys
import os
import logging
import hashlib

script_dir = os.path.dirname(os.path.realpath(__file__))
root_dir = os.path.dirname(script_dir)
sys.path.insert(0, root_dir)

# local imports
from cic_registry.pytest import *

logg = logging.getLogger(__name__)


@pytest.fixture(scope='session')
def config():
    config_dir = os.path.join(root_dir, 'config/test')
    conf = confini.Config(config_dir, 'CICTEST')
    conf.process()
    logg.debug('config {}'.format(conf))
    return conf
