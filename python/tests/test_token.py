# local imports
from cic_registry import CICRegistry


def test_token_gift(
        default_chain_registry,
        default_chain_spec,
        cic_registry,
        dummy_token_gifted,
        w3,
        ):

    c = CICRegistry.get_address(default_chain_spec, dummy_token_gifted)
    fn = c.function('balanceOf')
    fn(w3.eth.accounts[0]) == 100000000000000000000000
