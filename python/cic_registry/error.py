"""Contract Registry specific exceptions

.. moduleauthor:: Louis Holbrook <dev@holbrook.no>
"""
class AlreadyInitializedError(Exception):
    """Exception raised when registry already has been initialized
    """
    pass


class ContractExistsError(Exception):
    """Exception raised when trying to add a contract that already exists to the registry cache
    """
    pass


class RoleExistsError(Exception):
    """Exception raised when trying to add a role that already exists to the registry cache
    """
    pass


class UnknownContractError(Exception):
    """Exception raised when trying to access a contract that does not exist in the registry cache
    """
    pass


class UnknownChainError(Exception):
    """Exception raised when trying to access a chain registry that does not exist in the registry cache
    """
    pass


class ChainExistsError(Exception):
    """Exception raised when trying to add a chain registry that already exists in the registry cache
    """
    pass


class NotFinalizedError(Exception):
    """Exception raised when trying to load a cic registry that has not been sealed
    """
    pass


class UnknownDeclarationError(Exception):
    """Exception raised by declaration interpreter function if declaration is not understood
    """
    pass
