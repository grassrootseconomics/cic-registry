# local imports
from .contract import Contract


class Token(Contract):
    """Local representation of a single token instance in contract registry

    :param address: Contract address of token
    :type address: str, 0x-prefix
    :param contract: Token contract interface
    :type contract: web3.Contract object
    """

    def __init__(self, contract):
        name = contract.functions.name().call()
        super(Token, self).__init__(contract, name)


    def name(self):
        """Returns name of token

        :return: Name
        :rtype: str
        """
        return self.contract.functions.name().call()


    def symbol(self):
        """Returns ERC20 symbol of token

        :return: Symbol
        :rtype: str
        """
        return self.contract.functions.symbol().call()


    def decimals(self):
        """Returns number of decimals token is denominated in

        :return: Decimals
        :rtype: int
        """
        return self.contract.functions.decimals().call()
