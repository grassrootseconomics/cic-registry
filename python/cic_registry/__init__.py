"""Local Contract Registry cache

.. moduleauthor:: Louis Holbrook <dev@holbrook.no>

.. todo:: Extract abis and contract specs into separate package

.. todo:: Verify web3 as argument not needed for contract 

"""
from .registry import CICRegistry
from .registry import zero_address, zero_content, max_uint
#from .registry import from_text
#from .registry import to_text
from .registry import from_identifier
from .registry import to_identifier
