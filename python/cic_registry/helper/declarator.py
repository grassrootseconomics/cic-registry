# standard imports
import os
import json
import logging

# third-party imports
from cic_registry import CICRegistry
from cic_registry.error import UnknownContractError

logg = logging.getLogger()


class DeclaratorOracleAdapter:

    def __init__(self, contract, trusted_addresses, interface='Declarator'):
        self.contract = contract
        if len(trusted_addresses) == 0:
            raise ValueError('You seem to have trust issues. Declarator won\'t work without')
        self.trusted = trusted_addresses


    def open(self):
        """Establishes a connection to the underlying oracle
        """
        pass


    def close(self):
        """Frees connection resources
        """
        pass


    def get(self, search_address):
        return self.address_oracle(search_address)


    # TODO: WIP: This does no interpretation of the result, just assumes everything an ERC20 token
    def address_oracle(self, search_address):
        for trusted_address in self.trusted:
            r = self.contract.functions.declaration(trusted_address, search_address).call()
            if r == None or len(r) == 0:
                continue
            logg.warning('declaration check not implemented')
            return CICRegistry.abi('ERC20')
        raise UnknownContractError('No trust entries found for address {}'.format(trusted_address))
