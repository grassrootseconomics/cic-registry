# standard imports
import logging
import os
import json
import hashlib
import sys

# third-party imports
import pytest

# local imports
from cic_registry import CICRegistry

logg = logging.getLogger(__name__)

script_dir = os.path.dirname(os.path.realpath(__file__))


@pytest.fixture(scope='function')
def dummy_token_gifted(
        default_chain_spec,
        w3,
        init_eth_tester,
        dummy_token_registered,
        ):
 
    f = open(os.path.join(script_dir, '..', 'data', 'GiftableToken.json'), 'r')
    abi = json.load(f)
    f.close()

    contract = w3.eth.contract(address=dummy_token_registered, abi=abi)
    tx_hash = contract.functions.mint(100000000000000000000000).transact()

    init_eth_tester.mine_block()
    rcpt = w3.eth.getTransactionReceipt(tx_hash)

    return dummy_token_registered
  

@pytest.fixture(scope='function')
def dummy_token_registered(
        default_chain_spec,
        dummy_token,
        w3,
        ):

    f = open(os.path.join(script_dir, '..', 'data', 'GiftableToken.json'), 'r')
    abi = json.load(f)
    f.close()

    contract = w3.eth.contract(address=dummy_token, abi=abi)
    CICRegistry.add_token(default_chain_spec, contract)

    return dummy_token


@pytest.fixture(scope='function')
def dummy_token(
        default_chain_spec,
        w3,
        ):

    f = open(os.path.join(script_dir, '..', 'data', 'GiftableToken.bin'), 'r')
    bytecode = f.read()
    f.close()

    f = open(os.path.join(script_dir, '..', 'data', 'GiftableToken.json'), 'r')
    abi = json.load(f)
    f.close()

    t = w3.eth.contract(abi=abi, bytecode=bytecode)
    tx_hash = t.constructor('Dummytoken', 'DUM', 18).transact({
        'from': w3.eth.accounts[0],
        })
    rcpt = w3.eth.getTransactionReceipt(tx_hash)
    address = rcpt.contractAddress

    h = hashlib.new('sha256')
    h.update('DUM'.encode('utf-8'))
    z = h.digest()
    token_symbol_key = z.hex()

    try:
        c = CICRegistry.get_contract(default_chain_spec, 'TokenRegistry')
        fn = c.function('register')
        fn('0x' + token_symbol_key, address).transact({'from': w3.eth.accounts[0]})
    except ValueError as e:
        logg.warning('failed to add token to tokenregistry (did you add one?): {}'.format(e))

    return address
