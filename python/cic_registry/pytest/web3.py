# standard imports
import os
import logging

# third-party imports
import web3
from web3 import Web3, HTTPProvider
import eth_tester
import pytest
import confini

# local imports
from cic_registry.chain import ChainRegistry

logg = logging.getLogger(__name__)


class EthTesterCustom(eth_tester.EthereumTester):
    w3_mock = web3.Web3(web3.Web3.EthereumTesterProvider(eth_tester.MockBackend()))

    def __init__(self, backend):
        super(EthTesterCustom, self).__init__(backend)
        logg.debug('acounts {}'.format(self.get_accounts()))
        self.accounts = {}
        self.signer = self
        for pk in self.backend.account_keys:
            pubk = pk.public_key
            address = pubk.to_checksum_address()
            logg.debug('have pk {} pubk {} addr {}'.format(pk, pk.public_key, address))
            self.accounts[address] = pk


    def get(self, address, password=None):
        pk = self.accounts['0x' + address]._raw_key
        logg.debug('get pk {} from {}'.format(pk, address))
        return pk


@pytest.fixture(scope='session')
def init_eth_tester(
        ):
    # a hack to increase the gas limit, since our contracts take more gas than what's currently hardcoded in eth-tester/py-evm
    eth_params = eth_tester.backends.pyevm.main.get_default_genesis_params({
        'gas_limit': 9000000,
        })
    eth_backend = eth_tester.PyEVMBackend(eth_params)
    eth_tester_instance = EthTesterCustom(eth_backend)
    
    return eth_tester_instance


@pytest.fixture(scope='session')
def eth_provider(
        init_eth_tester,
        ):

    return web3.Web3.EthereumTesterProvider(init_eth_tester)


@pytest.fixture(scope='session')
def w3(
        eth_provider,
        init_eth_tester,
        ):

    w3 = web3.Web3(eth_provider)
    return w3


@pytest.fixture(scope='session')
def default_chain_registry(
        default_chain_spec,
        ):
    chain_registry = ChainRegistry(default_chain_spec)
    return chain_registry

