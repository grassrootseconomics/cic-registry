# standard imports
import logging
import os
import json

# third-party imports
import pytest

# local imports
from cic_registry import CICRegistry
from cic_registry import to_identifier
from cic_registry.contract import Contract

__script_dir = os.path.dirname(__file__)
__data_dir = os.path.join(__script_dir, '..', 'data')

logg = logging.getLogger()


@pytest.fixture(scope='session')
def registry_identifiers():
    return [
            'CICRegistry',
            'TokenRegistry',
            'AccountRegistry',
            'AddressDeclarator',
            ]

@pytest.fixture(scope='session')
def init_cic_registry(
        bloxberg_config,
        default_chain_registry,
        registry_identifiers,
        w3,
        ):

    constructor = w3.eth.contract(abi=CICRegistry.abi(), bytecode=CICRegistry.bytecode())

    logg.debug('initializing registry with identifiers: {}'.format(registry_identifiers))
    registry_identifiers_hex = list(map(to_identifier, registry_identifiers))

    tx_hash = constructor.constructor(registry_identifiers_hex).transact()
    r = w3.eth.getTransactionReceipt(tx_hash)
    cic_registry_address = r.contractAddress

    chain_identifier = to_identifier(default_chain_registry.chain())
    c = w3.eth.contract(abi=CICRegistry.abi(), address=cic_registry_address)
    c.functions.set(to_identifier('CICRegistry'), cic_registry_address, chain_identifier, bloxberg_config['digest']).transact()

    return cic_registry_address


@pytest.fixture(scope='session')
def cic_registry(
        default_chain_spec,
        default_chain_registry,
        init_cic_registry,
        w3,
    ):

    CICRegistry.init(w3, init_cic_registry)
    CICRegistry.add_chain_registry(default_chain_registry)
    CICRegistry.get_contract(default_chain_spec, 'CICRegistry')
    return init_cic_registry


@pytest.fixture(scope='session')
def solidity_abis():
    CICRegistry.add_path(os.path.join(__script_dir, '..', '..', 'tests', 'testdata', 'solidity'))

    contract_names = [
        'ERC20',
        'Registry',
            ]

    abi_objects = {}

    i = 0
    for n in contract_names:
        a = CICRegistry.abi(n)
        #filename = os.path.join(abi_path, '{}.json'.format(n))
        #f = open(filename, 'r')
        #a = json.load(f)
        #f.close()
        abi_objects[n] = a

    return abi_objects


@pytest.fixture(scope='session')
def evm_bytecodes():
    bin_path = os.path.join(__script_dir, '..', '..', 'tests', 'testdata', 'solidity')

    contract_names = [
        'TokenRegistry',
            ]

    bin_objects = {}

    i = 0
    for n in contract_names:
        filename = os.path.join(bin_path, '{}.bin'.format(n))
        f = open(filename, 'r')
        a = f.read()
        f.close()
        bin_objects[n] = a

    return bin_objects
