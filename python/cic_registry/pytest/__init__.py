import logging
logging.getLogger('eth.vm').setLevel(logging.WARNING)

# local imports
from cic_registry.chain import ChainSpec

# third-party imports
import pytest

__bloxberg_config_sha256 = '0xf0d55c546bdcc314bd860ee9cecd46cfad0c10bf75fdcad7ab79918426f05e5f'

# TODO: replace with actual config file data
__bloxberg_config_excerpt = {
    'name': 'Bloxberg',
    'params': {
        'networkID': 8995,
        },
        }


@pytest.fixture(scope='session')
def bloxberg_config(
        ):
    return {
            'digest': __bloxberg_config_sha256,
            'config': __bloxberg_config_excerpt,
            }


@pytest.fixture(scope='session')
def default_chain_spec(
        ):
    #return ChainSpec(__bloxberg_config_excerpt, 42)
    return ChainSpec('evm', 'bloxberg', 8995)


from .web3 import *
from .cic import *
from .token import *
