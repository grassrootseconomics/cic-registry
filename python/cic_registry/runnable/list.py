"""Set a new entry on CIC-registry

.. moduleauthor:: Louis Holbrook <dev@holbrook.no>
.. pgp:: 0826EDA1702D1E87C6E2875121D2E7BB88C2A746 

"""

# SPDX-License-Identifier: GPL-3.0-or-later

# standard imports
import os
import json
import argparse
import logging

# third-party imports
import web3
from crypto_dev_signer.eth.signer import ReferenceSigner as EIP155Signer
from crypto_dev_signer.keystore import DictKeystore
from crypto_dev_signer.eth.helper import EthTxExecutor
from chainlib.chain import ChainSpec

# local imports
from cic_registry import CICRegistry
from cic_registry import from_identifier
from cic_registry.chain import ChainSpec

logging.basicConfig(level=logging.WARNING)
logg = logging.getLogger()

logging.getLogger('web3').setLevel(logging.WARNING)
logging.getLogger('urllib3').setLevel(logging.WARNING)

script_dir = os.path.dirname(__file__)
data_dir = os.path.join(script_dir, '..', 'data')

argparser = argparse.ArgumentParser()
argparser.add_argument('-p', '--provider', dest='p', default='http://localhost:8545', type=str, help='Web3 provider url (http only)')
argparser.add_argument('-w', action='store_true', help='Wait for the last transaction to be confirmed')
argparser.add_argument('-ww', action='store_true', help='Wait for every transaction to be confirmed')
argparser.add_argument('-a', '--signer-address', dest='a', type=str, help='Accounts declarator owner')
argparser.add_argument('-r', '--registry-address', dest='r', required=True, type=str, help='cic registry address')
argparser.add_argument('-i', '--chain-spec', dest='i', required=True, type=str, help='chain spec')
argparser.add_argument('-d', default='0x00000000000000000000000000000000000000000000000000000000', type=str, help='chain config sha256 digest')
argparser.add_argument('-y', '--key-file', dest='y', type=str, help='Ethereum keystore file to use for signing')
argparser.add_argument('-v', action='store_true', help='Be verbose')
argparser.add_argument('-vv', action='store_true', help='Be more verbose')
argparser.add_argument('--abi-dir', dest='abi_dir', type=str, default=data_dir, help='Directory containing cic contract bytecode and abi (Default {})'.format(data_dir))
args = argparser.parse_args()

if args.v:
    logg.setLevel(logging.INFO)
elif args.vv:
    logg.setLevel(logging.DEBUG)

w3 = web3.Web3(web3.Web3.HTTPProvider(args.p))

chain_spec = ChainSpec.from_chain_str(args.i)
chain_id = chain_spec.network_id()


def main():
    CICRegistry.add_path(args.abi_dir)
    chain_spec = ChainSpec.from_chain_str(args.i)
    CICRegistry.init(w3, args.r)

    f = open(os.path.join(args.abi_dir, 'Registry.json'))
    abi = json.load(f)
    f.close()

    registry_contract = w3.eth.contract(address=args.r, abi=abi)

    i = 0
    identifiers = []
    #fn = registry_contract.function('identifiers')
    while True:
        #identifiers.append(fn(i).call())
        try:
            identifiers.append(registry_contract.functions.identifiers(i).call())
        except ValueError:
            break
        i += 1

    for identifier in identifiers:
        #print('{} {}'.format(from_identifier(identifier), CICRegistry.functions.
        address = registry_contract.functions.addressOf(identifier).call()
        print('{} {}'.format(from_identifier(identifier), address))


if __name__ == '__main__':
    main()
