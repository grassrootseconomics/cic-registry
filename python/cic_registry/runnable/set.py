"""Set a new entry on CIC-registry

.. moduleauthor:: Louis Holbrook <dev@holbrook.no>
.. pgp:: 0826EDA1702D1E87C6E2875121D2E7BB88C2A746 

"""

# SPDX-License-Identifier: GPL-3.0-or-later

# standard imports
import os
import json
import argparse
import logging

# third-party imports
import web3
from crypto_dev_signer.eth.signer import ReferenceSigner as EIP155Signer
from crypto_dev_signer.keystore import DictKeystore
from crypto_dev_signer.eth.helper import EthTxExecutor
from chainlib.chain import ChainSpec

# local imports
from cic_registry import CICRegistry
from cic_registry import to_identifier
from cic_registry.chain import ChainSpec

logging.basicConfig(level=logging.WARNING)
logg = logging.getLogger()

logging.getLogger('web3').setLevel(logging.WARNING)
logging.getLogger('urllib3').setLevel(logging.WARNING)

script_dir = os.path.dirname(__file__)
data_dir = os.path.join(script_dir, '..', 'data')

argparser = argparse.ArgumentParser()
argparser.add_argument('-p', '--provider', dest='p', default='http://localhost:8545', type=str, help='Web3 provider url (http only)')
argparser.add_argument('-w', action='store_true', help='Wait for the last transaction to be confirmed')
argparser.add_argument('-ww', action='store_true', help='Wait for every transaction to be confirmed')
argparser.add_argument('-a', '--signer-address', dest='a', type=str, help='Accounts declarator owner')
argparser.add_argument('-r', '--registry-address', dest='r', required=True, type=str, help='cic registry address')
argparser.add_argument('-i', '--chain-spec', dest='i', required=True, type=str, help='chain spec')
argparser.add_argument('-d', default='0x00000000000000000000000000000000000000000000000000000000', type=str, help='chain config sha256 digest')
argparser.add_argument('-k', required=True, type=str, help='contract identifier key')
argparser.add_argument('-y', '--key-file', dest='y', type=str, help='Ethereum keystore file to use for signing')
argparser.add_argument('-v', action='store_true', help='Be verbose')
argparser.add_argument('-vv', action='store_true', help='Be more verbose')
argparser.add_argument('--abi-dir', dest='abi_dir', type=str, default=data_dir, help='Directory containing cic contract bytecode and abi (Default {})'.format(data_dir))
argparser.add_argument('address', type=str, help='contract address')
args = argparser.parse_args()

if args.v:
    logg.setLevel(logging.INFO)
elif args.vv:
    logg.setLevel(logging.DEBUG)

w3 = web3.Web3(web3.Web3.HTTPProvider(args.p))

signer_address = None
keystore = DictKeystore()
if args.y != None:
    logg.debug('loading keystore file {}'.format(args.y))
    signer_address = keystore.import_keystore_file(args.y)
    logg.debug('now have key for signer address {}'.format(signer_address))
signer = EIP155Signer(keystore)

chain_spec = ChainSpec.from_chain_str(args.i)
chain_id = chain_spec.network_id()

helper = EthTxExecutor(
        w3,
        signer_address,
        signer,
        chain_id,
        block=args.ww,
    )


def main():
    CICRegistry.add_path(args.abi_dir)
    chain_spec = ChainSpec.from_chain_str(args.i)
    CICRegistry.init(w3, args.r)

    contract_identifier = to_identifier(args.k)
    chain_spec_identifier = to_identifier(str(chain_spec))
    (tx_hash, rcpt) = helper.sign_and_send(
            [
                CICRegistry.contract.functions.set(contract_identifier, args.address, chain_spec_identifier, args.d).buildTransaction,
                ],
                force_wait=True,
            )

    print(tx_hash)


if __name__ == '__main__':
    main()
