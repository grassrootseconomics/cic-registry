# standard imports
import logging

logg = logging.getLogger(__name__)


class Contract:
    """Local representation of a single contract instance in contract registry
   
    .. todo::

    :param address: Address of contract
    :type address: str, 0x-prefix
    :param address: Contract interface
    :type contract: web3.Contract object
    """
    def __init__(self, contract, identifier):
        self.contract = contract
        self.__identifier = identifier


    def identifier(self):
        return self.__identifier


    def abi(self):
        return self.contract.abi


    def address(self):
        return self.contract.address


    def gas(self, function, data=None):
            """Retrieve cached gas limit value for given contract transaction function.

            Delegates to web3.eth.estimateGas if no cached value exists.

            :param function: Function solidity name (not function signature hash) to report gas usage for
            :type function: str
            :param data: Data that will be sent with transaction
            :type data: str, 0x-hex
            :return: Suggested gas limit
            :rtype: int
            """
            gas_max = 8000000
            logg.warning('gas budget is not implemented, replying "gas max" {} to everything'.format(8000000))
            return gas_max


    def function(self, function_name):
        """Retrieve a contract function interface

        :param contract_key: Local id of contract. If the contract exists in the Contract Registry, the id will match the name used there.
        :type contract_key: str
        :param function_name: Contract function name
        :type function_name: str
        :return: Function interface
        :rtype: <web3.Contract> function
        """
        return getattr(self.contract.functions, function_name)


