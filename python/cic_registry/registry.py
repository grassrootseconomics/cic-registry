"""Contract Registry cache content objects
        logg.debug('cicregistry chain spec {}'.format(CICRegistry.chain_spec.chain_id()))

.. moduleauthor:: Louis Holbrook <dev@holbrook.no>
"""
# standard imports
import os
import logging
import json
import hashlib

# third-party imports
import web3

# local imports
from .chain import ChainSpec
from .chain import ChainRegistry
from .error import ContractExistsError
from .error import AlreadyInitializedError
from .error import UnknownContractError
from .error import UnknownChainError
from .error import ChainExistsError
from .contract import Contract
from .const import (
        zero_address,
        zero_content,
        max_uint,
        )

logg = logging.getLogger(__name__)

moddir = os.path.dirname(__file__)
datadir = os.path.join(moddir, 'data')

tmp_abi_translation = {
        'AccountRegistry': 'Registry',
        'TokenRegistry': 'Registry',
        'TransferApproval': 'TransferAuthorization',
        }


class CICRegistry:

    paths = []
    finalized = False
    contract = None
    default_chain_spec = None
    datadir = datadir
    w3 = None
    __chains_registry = {}

    __abi = None
    __bytecode = None


    @staticmethod
    def init(w3, address, default_chain_spec=None):
        CICRegistry.w3 = w3
        abi = CICRegistry.abi()
        CICRegistry.contract = w3.eth.contract(abi=abi, address=address)
        logg.info('CICRegistry initialized with address {}'.format(address))
        if default_chain_spec != None:
            CICRegistry.default_chain_spec = default_chain_spec
            logg.info('CICRegistry initialized with chain spec {}'.format(str(default_chain_spec)))


    @staticmethod
    def load_for(chain_spec):
        chain_str = str(chain_spec)
        registry_contract = CICRegistry.get_contract(chain_spec, 'CICRegistry')
        fn = registry_contract.function('identifiers')
        i = 0
        identifiers = []
        while True:
            try:
                identifier = fn(i).call()
                identifier_delimited = identifier[0:identifier.find(b'\x00')]
                identifiers.append(identifier_delimited.decode('utf-8'))
                i += 1
            except ValueError as e:
                logg.debug('name in index {} not found {}'.format(i, e))
                break

        for identifier in identifiers:
            logg.debug('poking registry identifier {}'.format(identifier))
            interface = tmp_abi_translation.get(identifier)
            if interface == None:
                interface = identifier
            try:
                CICRegistry.get_contract(chain_spec, identifier, interface)
            except ValueError:
                logg.warning('registry identifier {} does not have an entry for chain {}'.format(identifier, chain_str))


    @staticmethod
    def add_chain_registry(chain_registry, default_chain=False):
        chain_str = chain_registry.chain()
        if CICRegistry.__chains_registry.get(chain_str) != None:
            logg.debug('skip add chain registry {}'.format(chain_str))
            raise ChainExistsError(chain_str)
        CICRegistry.__chains_registry[chain_str] = chain_registry
        if default_chain or CICRegistry.default_chain_spec == None:
            logg.info('CICRegistry default chain set to {}'.format(chain_str))
            CICRegistry.default_chain_spec = chain_registry.chain_spec


    @staticmethod
    def get_contract_address(chain_spec, identifier):
        k = to_identifier(identifier)
        #contract_address = CICRegistry.contract.functions.addressOf(registry_identifiers[identifier]).call()
        contract_address = CICRegistry.contract.functions.addressOf(k).call()
        if contract_address == zero_address:
            raise ValueError('unknown contract at identifier: {}'.format(identifier))
        return contract_address


    @staticmethod
    def get_contract(chain_spec, identifier, interface=None):
        if interface == None:
            interface = identifier
        chain_str = str(chain_spec)
        contract = CICRegistry.__chains_registry[chain_str].contracts.get(identifier)
        if contract == None:
            logg.debug('contract {} not found in cache, checking on-chain {}'.format(identifier, chain_str))
            contract_address = CICRegistry.get_contract_address(chain_spec, identifier)
            k = to_identifier(identifier)
            abi = CICRegistry.abi(interface)
            c = CICRegistry.w3.eth.contract(abi=abi, address=contract_address)
            CICRegistry.add_contract(chain_spec, c, identifier)
            contract = CICRegistry.get_contract(chain_spec, identifier)

        return contract


    @staticmethod
    def get_token(chain_spec, symbol):
        chain_str = str(chain_spec)
        token = None
        try:
            token = CICRegistry.__chains_registry[chain_str].get_token_by_symbol(symbol)
        except KeyError:
            c = CICRegistry.get_contract(chain_spec, 'TokenRegistry', 'Registry')
            fn = c.function('addressOf')
            h = hashlib.new('sha256')
            h.update(symbol.encode('utf-8'))
            z = h.digest()
            symbol_key = z.hex()
            token_address = fn('0x' + symbol_key).call()
            ct = CICRegistry.w3.eth.contract(abi=CICRegistry.abi('ERC20'), address=token_address)
            token = CICRegistry.add_token(chain_spec, ct)

        return token


    @staticmethod
    def get_address(chain_spec, address):
        chain_str = str(chain_spec)
        chain_registry = CICRegistry.__chains_registry[chain_str]
        contract = chain_registry.get_address(address)
        if contract == None:
            abi = chain_registry.search(address)
            c = CICRegistry.w3.eth.contract(abi=abi, address=address)
            istoken = False
            try:
                symbol = c.functions.symbol().call()
                logg.debug('address {} is token {}'.format(address, symbol))
                istoken = True
            except Exception as e:
                logg.debug('asserting {} as token failed: {}'.format(e))
                pass
            if not istoken:
                raise AttributeError('{} is not a token on chain {}'.format(address, str(chain_spec)))
            contract = CICRegistry.add_token(chain_spec, c)
        return contract
    

    @staticmethod
    def get_chain_registry(chain_spec):
        chain_str = str(chain_spec)
        return CICRegistry.__chains_registry[chain_str]


    @staticmethod
    def add_contract(chain_spec, contract, identifier):
        chain_str = str(chain_spec)
        c = CICRegistry.__chains_registry[chain_str].add_contract(contract, identifier)
        logg.info('registry added contract {}:{} ({})'.format(str(chain_spec), identifier, c.address()))
        return c


    @staticmethod
    def add_token(chain_spec, contract):
        chain_str = str(chain_spec)
        t = CICRegistry.__chains_registry[chain_str].add_token(contract)
        logg.info('registry added token {}:{} ({})'.format(str(chain_spec), t.symbol(), t.address()))
        return t


    @staticmethod
    def get_role(chain_spec, identifier):
        chain_str = str(chain_spec)
        return CICRegistry.__chains_registry[chain_str].get_role(identifier)


    @staticmethod
    def add_role(chain_spec, address, identifier, missing_contract_ok=False):
        chain_str = str(chain_spec)
        if not missing_contract_ok:
            # It's enough to confirm that it exists, this will raise exception if not
            CICRegistry.get_contract_address(chain_spec, identifier)
        logg.info('setting registry account for {} to {}'.format(address, identifier))
        CICRegistry.__chains_registry[chain_str].add_role(address, identifier)


    @staticmethod
    def abi(contract_identifier='CICRegistry'):
        if contract_identifier == 'CICRegistry':
            if CICRegistry.__abi == None:
                f = open(os.path.join(datadir, 'Registry.json'), 'r')
                CICRegistry.__abi = json.load(f)
                f.close()
            return CICRegistry.__abi
        filename = '{}.json'.format(contract_identifier)
        p = CICRegistry.search_path(filename)
        if p == None:
            raise FileNotFoundError('solidity abi: {}'.format(filename))
        f = open(os.path.join(CICRegistry.datadir, p), 'r')
        abi = json.load(f)
        f.close()
        return abi


    @staticmethod
    def bytecode(contract_identifier='CICRegistry'):
        if contract_identifier == 'CICRegistry':
            if CICRegistry.__bytecode == None:
                f = open(os.path.join(datadir, 'Registry.bin'), 'r')
                CICRegistry.__bytecode = f.read()
                f.close()
            return CICRegistry.__bytecode
        filename = '{}.bin'.format(contract_identifier)
        p = CICRegistry.search_path(filename)
        if p == None:
            raise FileNotFoundError('evmbytecode: {}'.format(filename))
        f = open(os.path.join(CICRegistry.datadir, p), 'r')
        bytecode = f.read()
        f.close()
        return bytecode

    
    @staticmethod
    def add_path(path):
        if not os.path.isdir(path):
            logg.error('registry path {} is not a directory'.format(path))
        logg.debug('added registry path {}'.format(path))
        CICRegistry.paths.append(path)


    def search_path(filename):
        logg.debug('searching for {}'.format(filename))
        for p in CICRegistry.paths:
            found = os.path.join(p, filename)
            try:
                os.stat(found)
                logg.debug('using {} in {}'.format(filename, p))
                return found
            except FileNotFoundError:
                logg.debug('file {} not found in {}'.format(found, p))
        return None


def to_text(b):
        b = b[:b.find(0)]
        return web3.Web3.toText(b)


def from_text(txt):
    return '0x{:0<64s}'.format(txt.encode('utf-8').hex())


def from_identifier(b):
    return to_text(b)


def to_identifier(txt):
    return from_text(txt)
