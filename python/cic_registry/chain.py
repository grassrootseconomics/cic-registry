# standard imports
import logging
import os
import json

# local imports
from .error import ContractExistsError
from .error import UnknownContractError
from .error import RoleExistsError
from .token import Token
from .role import Role
from .contract import Contract

logg = logging.getLogger()

script_dir = os.path.dirname(os.path.realpath(__file__))
data_dir = os.path.join(script_dir, 'data')


class ChainSpec:

    def __init__(self, engine, common_name, network_id, tag=None):
        self.o = {
                'engine': engine,
                'common_name': common_name,
                'network_id': network_id,
                'tag': tag,
                }

    def network_id(self):
        return self.o['network_id']


    def chain_id(self):
        return self.o['network_id']


    def engine(self):
        return self.o['engine']


    def common_name(self):
        return self.o['common_name']


    # legacy support
    def chain_name(self):
        return self.o['common_name']


    @staticmethod
    def from_chain_str(chain_str):
        o = chain_str.split(':')
        if len(o) < 3:
            raise ValueError('Chain string must have three sections, got {}'.format(len(o)))
        tag = None
        if len(o) == 4:
            tag = o[3]
        return ChainSpec(o[0], o[1], int(o[2]), tag)


    def __str__(self):
        s = '{}:{}:{}'.format(self.o['engine'], self.o['common_name'], self.o['network_id'])
        if self.o['tag'] != None:
            s += ':' + self.o['tag']
        return s


class ChainRegistry:
    """Caches all relevant contract objects for fast access
    """

    def __init__(self, chain_spec):
        self.addresses = {}
        self.contracts = {}
        self.tokens = {}
        self.roles = {}
        self.oracles = {}
        self.erc20_abi = None
        self.loaded = False
        self.gas_provider = None
        self.chain_spec = chain_spec

        f = open(os.path.join(data_dir, 'ERC20.json'), 'r')
        abi = json.load(f)
        f.close()
        self.erc20_abi = abi


    def chain(self):
        return str(self.chain_spec)


    def get_role(self, identifier):
        return self.roles.get(identifier)


    def get_address(self, address):
        """Return contract by address

        :return: Contract if found 
        :rtype: Contract or None
        """
        return self.addresses.get(address.lower())


    def cache_token_count(self):
        """Return number of tokens in registry cache

        :return: Number of tokens
        :rtype: int
        """
        return len(self.tokens)


    def get_token_by_symbol(self, symbol):
        """Look up token address by symbol

        :param symbol: Symbol
        :type symbol: str
        :return: Address
        :rtype: str, 0x-hex
        """
        return self.tokens[symbol.upper()]
        
    
    def get_contract(self, contract_name):
        """Get contract interface by Contract Registry name

        The name is an enumerated, well-known value defined by the ContractRegistryClient contract.
        :param contract_name: Name
        :type contract_name: str
        :return: Contract interface
        :rtype: web3.Contract
        """
        return self.contracts[contract_name]


    def abi(self, contract_key):
        """Return the EVM abi of given contract

        :param contract_key: Local id of contract. If the contract exists in the Contract Registry, the id will match the name used there.
        :type contract_key: str
        :return: ABI, json
        :rtype: dict
        """
        if contract_key == 'ERC20':
            return self.erc20_abi

        return self.contracts[contract_key].contract.abi


    def search(self, address):
        for k in self.oracles.keys():
            logg.debug('looking up {} in oracle "{}"'.format(address, k))
            try:
                self.oracles[k].open()
                abi = self.oracles[k].get(address)
                self.oracles[k].close()
            except FileNotFoundError:
                continue
            logg.debug('match for {} in oracle "{}"'.format(address, k))
            return abi
        raise UnknownContractError('{}:{}'.format(str(self.chain_spec), address))


    def add_role(self, address, identifier):
        contract_address = self.addresses.get(address)
        if contract_address:
            raise RoleExistsError('role {} already registered on address {}'.format(identifier, address))
        r = Role(address, identifier)
        self.addresses[address.lower()] = r
        self.roles[identifier] = r
        return r


    def add_token(self, contract):
        if self.addresses.get(contract.address):
            raise ContractExistsError('token {} already registered'.format(contract.address))
        t = Token(contract)
        self.addresses[t.address().lower()] = t
        self.tokens[t.symbol().upper()] = t
        return t


    def add_contract(self, contract, identifier):
        if self.addresses.get(contract.address):
            raise ContractExistsError('token {} already registered'.format(contract.address))
        c = Contract(contract, identifier)
        self.addresses[c.address().lower()] = c
        self.contracts[identifier] = c
        return c


    def add_oracle(self, oracle, identifier):
        self.oracles[identifier] = oracle
