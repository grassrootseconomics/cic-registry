# local imports
from .const import zero_content
from .error import UnknownDeclarationError


class Declaration:

    def __init__(self, address, filters):
        self.address = address
        self.name = str(address)
        self.filters = filters
        self.sources = {}
        self.items = {}


    def set_name(self, name):
        self.name = name


    def add_source(self, declarator, srcs):
        for src in srcs:
            self.add_to_source(declarator, src)
        if len(self.items) == 0:
            raise UnknownDeclarationError('no valid declarations found')


    def add_to_source(self, declarator, src):
        if src == zero_content:
            raise ValueError('attempt to add zero-content to declarator sources')

        o = None
        for f in self.filters:
            try:
                o = f(src)
                break
            except UnknownDeclarationError:
                continue

        if o == None:
            return

        if self.sources.get(declarator) == None:
            self.sources[declarator] = []
            self.items[declarator] = []

        self.sources[declarator].append(src)
        self.items[declarator].append(o)
    

class TokenDeclaration(Declaration):
   
    def add_token_source(self, declarator, srcs):
        self.add_source(declarator, srcs)


# Magic follows below here

def token_filters():
    return []


def to_token_declaration(declarator, address, srcs, filters=None):
    if filters == None:
        filters = token_filters()
    t = TokenDeclaration(address, filters)
    t.add_token_source(declarator, srcs)
    return t
